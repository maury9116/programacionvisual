﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AmazonItsco_Logica_ClassLibrary;
using AmazonItsco_Data_ClassLibrary;

namespace AmazonItsco_WebForms.Public.Auth
{
    public partial class wfmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            Usuario usuario = new Usuario();
            usuario = LogicaUsuario.getUserbyUsernameAndPassword(username, password);
            if (usuario != null)
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                Response.Write("<script>alert('Usuario o Clave Incorrecta');</script>");
            }
        }
    }
}