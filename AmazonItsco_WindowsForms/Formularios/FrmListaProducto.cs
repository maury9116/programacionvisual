﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmazonItsco_Logica_ClassLibrary;
using AmazonItsco_Data_ClassLibrary;

namespace AmazonItsco_WindowsForms.Formularios
{
    public partial class FrmListaProducto : Form
    {

        public delegate void loadNameProduct(Producto producto);
        public event loadNameProduct eventLoadNameProduct;
        private Producto dataProducto;

        public FrmListaProducto()
        {
            InitializeComponent();
        }

        private void FrmListaProducto_Load(object sender, EventArgs e)
        {
            var listaProduct = LogicaProducto.getAllProducts();
            loadProducts(listaProduct);
        }

        private void loadProducts(List<Producto> listaProduct)
        {
            try
            {
                //var listaProduct = LogicaProducto.getAllProducts();
                if (listaProduct != null && listaProduct.Count > 0)
                {
                    gdvProducto.DataSource = listaProduct.Select(data => new
                    {
                        Codigo = data.pro_codigo,
                        Nombre = data.pro_nombre,
                        Categoria = data.Categoria.cat_descripcion,
                        Precio_Venta = data.pro_precioventa
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al obtener productos jaja" + ex.Message);
            }
        }

        private void gdvProducto_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var codigoProducto = gdvProducto.Rows[e.RowIndex].Cells["Codigo"].Value;
            if (!string.IsNullOrEmpty(codigoProducto.ToString()))
            {
                Producto producto = new Producto();
                producto = LogicaProducto.getProductByCode(codigoProducto.ToString());
                if (producto != null)
                {
                    dataProducto = new Producto();
                    dataProducto = producto;
                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void buscar(string datoBuscar)
        {
            if (!string.IsNullOrEmpty(datoBuscar))
            {
                List<Producto> listaProductos = new List<Producto>();
                listaProductos = LogicaProducto.getSearchAllProducts(datoBuscar);
                loadProducts(listaProductos);
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            if (dataProducto != null)
            {
                eventLoadNameProduct(dataProducto);
                this.Close();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dataProducto != null)
            {
                FrmProducto frmProducto = new FrmProducto(dataProducto.pro_id);
                frmProducto.Show(); 
            }
            else
            {
                MessageBox.Show("Debe seleccionar un producto jaja");
            }


        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmProducto frmProducto = new FrmProducto(0);
            frmProducto.Show();
        }
    }
}
