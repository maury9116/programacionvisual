﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmazonItsco_Logica_ClassLibrary;
using AmazonItsco_Data_ClassLibrary;

namespace AmazonItsco_WindowsForms.Formularios
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void login()
        {
            string correo = txtCorreo.Text.TrimEnd();
            string clave = txtClave.Text;

            if (!string.IsNullOrEmpty(correo) && !string.IsNullOrEmpty(clave))
            {
                Usuario usuario = new Usuario();
                usuario = LogicaUsuario.getUserbyUsernameAndPassword(correo, clave);
                if (usuario != null)
                {
                    MessageBox.Show("Bienvenido al sistema.\n" + usuario.Perfil.pef_descripcion + "\n" +
                        " " + usuario.Persona.per_apellidos , "Sistema Amazon Itsco", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    FrmMaster frmMaster = new FrmMaster();
                    frmMaster.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Correo o clave incorrecta", "Sistema Amazon Itsco", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Correo o clave. Campos Obligatorios", "Sistema Amazon Itsco", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            login();
        }
    }
}
