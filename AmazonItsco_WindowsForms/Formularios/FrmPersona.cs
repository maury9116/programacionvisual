﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmazonItsco_Data_ClassLibrary;
using AmazonItsco_Logica_ClassLibrary;


namespace AmazonItsco_WindowsForms.Formularios
{
    public partial class FrmPersona : Form
    {
        public FrmPersona()
        {
            InitializeComponent();
            loadPerson();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            updatePersona();
        }


        private void savePersona()
        {
            try
            {
                Persona persona = new Persona();
                persona.per_tipodni = cmbTipoDNI.Text == "Cedula" ? "C" : "P";
                persona.per_dni = txtDNI.Text.TrimEnd().TrimStart();
                persona.per_apellidos = txtApellidos.Text.ToUpper();
                persona.per_nombres = txtNombres.Text.ToUpper();
                persona.per_genero = cmbGenero.Text == "Masculino" ? Convert.ToChar("M") : Convert.ToChar("F");
                persona.per_fechanacimiento = dtpFechaNacimiento.Value;

                bool res = LogicaPersona.savePersona(persona);
                if (res)
                {
                    MessageBox.Show("Registro guardado correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar persona jaja" + ex.Message);
            }
        }

        private void loadPerson()
        {
            try
            {

                Persona persona = new Persona();

                persona = LogicaPersona.getPersonbyId(5);

                if (persona != null)
                {
                    string tipoDni = persona.per_tipodni == "C" ? "Cedula" : "DNI";

                    lblCodigo.Text = persona.per_id.ToString();
                    cmbTipoDNI.SelectedIndex = cmbTipoDNI.FindString(tipoDni);
                    txtDNI.Text = persona.per_dni;
                    txtApellidos.Text = persona.per_apellidos;
                    txtNombres.Text = persona.per_nombres;

                    string genero = persona.per_genero == Convert.ToChar("M") ? "Masculino" : "Femenino";


                    cmbGenero.SelectedIndex = cmbGenero.FindString(genero);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar persona jaja" + ex.Message);
            }

        }

        private void updatePersona()
        {
            try
            {
                int codigoPersona = Convert.ToInt32(lblCodigo.Text);

                if (!string.IsNullOrEmpty(lblCodigo.Text))
                {
                    Persona persona = new Persona();
                    persona = LogicaPersona.getPersonbyId(codigoPersona);
                    persona.per_tipodni = cmbTipoDNI.Text == "Cedula" ? "C" : "P";
                    persona.per_dni = txtDNI.Text.TrimEnd().TrimStart();
                    persona.per_apellidos = txtApellidos.Text.ToUpper();
                    persona.per_nombres = txtNombres.Text.ToUpper();
                    persona.per_genero = cmbGenero.Text == "Masculino" ? Convert.ToChar("M") : Convert.ToChar("F");
                    persona.per_fechanacimiento = dtpFechaNacimiento.Value;

                    bool res = LogicaPersona.updatePersona(persona);
                    if (res)
                    {
                        MessageBox.Show("Registro actualizado correctamente");
                    } 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualizar persona jaja" + ex.Message);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            savePersona();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }
    }
}
