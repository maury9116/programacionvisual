﻿using AmazonItsco_Data_ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonItsco_Logica_ClassLibrary
{
    public class LogicaProducto
    {
        private static dcAmazonItscoDataContext dc = new dcAmazonItscoDataContext();

        public static List<Producto> getAllProducts()
        {
            try
            {
                var resProduct = dc.Producto.Where(data => data.pro_status.Equals("A"));
                return resProduct.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos" + ex.Message);
            }
        }

        public static List<Producto> getSearchAllProducts(string codeProduct)
        {
            try
            {
                var resProduct = dc.Producto.Where(data => data.pro_status.Equals("A")
                                                   && data.pro_codigo.StartsWith(codeProduct));
                return resProduct.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos" + ex.Message);
            }
        }

        public static Producto getProductById(int idProduct)
        {
            try
            {
                var resProduct = dc.Producto.Where(data => data.pro_status.Equals("A")
                                                   && data.pro_id.Equals(idProduct)).
                                                   FirstOrDefault();
                return resProduct;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos" + ex.Message);
            }
        }

        public static Producto getProductByCode(string codeProduct)
        {
            try
            {
                var resProduct = dc.Producto.Where(data => data.pro_status.Equals("A")
                                                   && data.pro_codigo.Equals(codeProduct)).
                                                   FirstOrDefault();
                return resProduct;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos" + ex.Message);
            }
        }


        public static bool saveProdct(Producto producto)
        {
            try
            {

                bool res = false;
                producto.pro_status = 'A';
                producto.pro_add = DateTime.Now;

                dc.Producto.InsertOnSubmit(producto);
                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al guardar producto" + ex.Message);
            }

        }

        public static bool updateProdct(Producto producto)
        {
            try
            {
                dc = new dcAmazonItscoDataContext();

                bool res = false;
                producto.pro_edit = DateTime.Now;

                var resUpdate = dc.Pcd_UpdateProduct(producto.pro_id, producto.pro_codigo,
                                                producto.pro_nombre, producto.pro_descripcion,
                                                producto.pro_peso, producto.pro_preciocompra,
                                                producto.pro_precioventa, producto.pro_edit,
                                                producto.cat_id);

                var resProduct = resUpdate.FirstOrDefault<Pcd_UpdateProductResult>();
                int resultado = resProduct.Resultado;
                if (resultado>0)
                {
                    res = true;
                }

                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al modificar producto" + ex.Message);
            }

        }


        public static bool updateProdct2(Producto producto)
        {
            try
            {
                bool res = false;
                producto.pro_edit = DateTime.Now;
                dc.ExecuteCommand("UPDATE [dbo].[Producto] " +
                                    "SET [pro_codigo] = {0} " +
                                    ",[pro_nombre] = {1} " +
                                    ",[pro_descripcion] = {2} " +
                                    ",[pro_peso] = {3} " +
                                    ",[pro_preciocompra] = {4} " +
                                    ",[pro_precioventa] = {5} " +
                                    ",[pro_edit] = {6} " +
                                    ",[cat_id] = {7} " +
                                    "WHERE [pro_id] = {8}", new object[] {
                                        producto.pro_codigo,
                                        producto.pro_nombre,
                                        producto.pro_descripcion,
                                        producto.pro_peso,
                                        producto.pro_preciocompra,
                                        producto.pro_precioventa,
                                        producto.pro_edit,
                                        producto.cat_id,
                                        producto.pro_id
                                    });

                //Refresh del contexto de datos
                dc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, dc.Usuario);
                //Commit de la base
                dc.SubmitChanges();
                res = true;
                return res;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al modificar producto" + ex.Message);
            }

        }

    }
}
