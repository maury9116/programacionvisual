﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmazonItsco_Data_ClassLibrary;

namespace AmazonItsco_Logica_ClassLibrary
{
    public class LogicaUsuario
    {

        private static dcAmazonItscoDataContext dc = new dcAmazonItscoDataContext();

        public static Usuario getUserbyUsernameAndPassword(string correo, string clave)
        {
            try
            {
                var resUser = dc.Usuario.Where(data => data.usu_correo.Equals(correo)
                                                    && data.usu_clave.Equals(clave)
                                                    && data.usu_status.Equals("A")).
                                              FirstOrDefault();

                //Select * from Usuario u inner join Perfil p
                //on u.pef_id=p.pef_id
                //where usu_correo='correo' and 
                //usu_clave='clave'
                //usu_status='A'

                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener usuario");
            }


        }


        public static bool updateUsuario(Usuario usuario)
        {
            try
            {
                bool res = false;
                usuario.usu_edit = DateTime.Now;
                //Actualizar contexto de datos commit
                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al modificar personas" + ex.Message);
            }


        }


    }
}
