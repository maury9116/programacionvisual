﻿using AmazonItsco_Data_ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonItsco_Logica_ClassLibrary
{
    public class LogicaPersona
    {
        private static dcAmazonItscoDataContext dc = new dcAmazonItscoDataContext();

        public static List<Persona> getAllPerson()
        {
            try
            {
                var resUser = dc.Persona.Where(data => data.per_status.Equals("A"));
                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }


        }

        public static Persona getPersonbyId(int codigo)
        {
            try
            {
                var resUser = dc.Persona.Where(data => data.per_status.Equals("A")
                                               && data.per_id.Equals(codigo)).FirstOrDefault();
                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }


        }


        public static List<Persona> getAllPersonByDni(string cedula)
        {
            try
            {
                var resUser = dc.Persona.Where(data => data.per_status.Equals("A")
                                                     && data.per_dni.StartsWith(cedula));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }


        }

        public static List<Persona> getAllPersonByApellidos(string apellidos)
        {
            try
            {
                var resUser = dc.Persona.Where(data => data.per_status.Equals("A")
                                                     && data.per_apellidos.StartsWith(apellidos));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }


        }

        public static bool savePersona(Persona persona)
        {
            try
            {

                bool res = false;
                persona.per_status = 'A';
                persona.per_add = DateTime.Now;
                //persona.per_delete = DateTime.Now;
                //persona.per_edit = DateTime.Now;

                dc.Persona.InsertOnSubmit(persona);
                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al guardar personas" + ex.Message);
            }

        }


        public static bool updatePersona(Persona persona)
        {
            try
            {
                bool res = false;
                persona.per_edit = DateTime.Now;
                //Actualizar contexto de datos commit
                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al modificar personas" + ex.Message);
            }


        }

        public static bool deletePersona(Persona persona)
        {
            try
            {
                bool res = false;
                persona.per_delete = DateTime.Now;
                persona.per_status = 'I';
                //Actualizar contexto de datos commit
                dc.SubmitChanges();

                res = true;
                //SELECT * FROM Persona WHERE per_status='A' and per_dni like 'cedula%';
                return res;

            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al eliminar personas" + ex.Message);
            }


        }

    }
}
